import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    hide = true;
   username = '';
   password = '';
   invalidIds = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


  public onSubmit(): void {
    console.log(this.username);
    console.log(this.password);

    if(this.username == 'admin' && this.password == 'admin'){
      this.router.navigate(["/admin"])
    } else {
      this.invalidIds = true;
    }
  }
}
