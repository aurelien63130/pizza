import { Component, OnInit } from '@angular/core';
import {Pizza} from "../../../models/pizza";
import {FormControl, Validators} from "@angular/forms";
import {PizzaServiceService} from "../../../services/pizza-service.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  pizza: Pizza = new Pizza();
  baseControl = new FormControl('', Validators.required);
  loading: boolean = false;

  constructor(private pizzaService: PizzaServiceService, private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onSubmit(): void{
    this.loading = true;
    this.pizza.base = this.baseControl.value;
    this.pizzaService.add(this.pizza).subscribe(data => {
      this.router.navigate(['/admin/pizzas']);
      this.snackBar.open('Pizza ajoutée !!', 'Fermer', {
        horizontalPosition: 'end',
        verticalPosition: 'top',
        duration: 3000
      });
      this.loading = false;

    })
  }
}
