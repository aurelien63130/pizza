import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Pizza} from "../../../models/pizza";
import {PizzaServiceService} from "../../../services/pizza-service.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {

  loading: boolean = true;
  displayedColumns: string[] = ['nom', 'ingredient1', 'ingredient2', 'ingredient3', 'photo', 'action'];
  dataSource: MatTableDataSource<Pizza> = new MatTableDataSource<Pizza>([]);



  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private pizzaService: PizzaServiceService, private snackBar: MatSnackBar) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  ngOnInit(): void {
    this.pizzaService.getAll().subscribe(data => {
      this.dataSource.data = data;
      this.loading = false;
    })
  }

  remove(id: number){
    this.loading = true;
    this.pizzaService.remove(id).subscribe(data => {
      this.ngOnInit();
      this.snackBar.open('Pizza supprimée !!', 'Fermer', {
        horizontalPosition: 'end',
        verticalPosition: 'top',
        duration: 3000
      });

    });
  }

}
