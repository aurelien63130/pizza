import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Pizza} from "../models/pizza";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PizzaServiceService {

  apiUrl = environment.apiUrl+ "/pizzas";

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Pizza[]>{
    return this.httpClient.get<Pizza[]>(this.apiUrl)
  }

  add(pizza: Pizza): Observable<Pizza> {
    return this.httpClient.post<Pizza>(this.apiUrl,pizza);
  }

  remove(id: number): Observable<Pizza>{
    return  this.httpClient.delete(this.apiUrl + '/' + id);
  }
}
