export class Pizza {
  ingredient1?: string;
  ingredient2?: string;
  ingredient3?: string;
  base?: string;
  nom?: string;
  description?: string;
  photo?: string;
}
