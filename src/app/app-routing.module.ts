import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {AdminListComponent} from "./components/admin/admin-list/admin-list.component";
import {AdminComponent} from "./components/admin/admin/admin.component";
import {AddComponent} from "./components/admin/add/add.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent, children: [
      {
        path: 'pizzas', // child route path
        component: AdminListComponent, // child route component that the router renders
      },
      {
        path: 'add', // child route path
        component: AddComponent, // child route component that the router renders
      }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
